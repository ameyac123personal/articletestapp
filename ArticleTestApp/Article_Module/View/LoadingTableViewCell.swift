//
//  LoadingTableViewCell.swift
//  ArticleTestApp
//
//  Created by Ameya on 09/05/20.
//  Copyright © 2020 JTVC. All rights reserved.
//

import UIKit

class LoadingTableViewCell: UITableViewCell {
    
    @IBOutlet var loadingLabel : UILabel!
    @IBOutlet var activityIndicator : UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
