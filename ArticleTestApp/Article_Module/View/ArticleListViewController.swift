//
//  ViewController.swift
//  ArticleTestApp
//
//  Created by Ameya on 08/05/20.
//  Copyright © 2020 JTVC. All rights reserved.
//

import UIKit
import SDWebImage
import Network

let articleCellIdentifier = "ArticleTableViewCell"
let loadingCellIdentifier = "LoadingTableViewCell"

let articleTableViewCellEstimatedHeight = CGFloat(375.0)
let loadingTableViewCellEstimatedHeight = CGFloat(44.0)

let endOfLoadingText = "End of Articles."
let loadingText = "Loading Article Page"

class ArticleListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    /*
     Section Type
     */
    enum Section : Int{
        case articleSection,
        loadingSection
    }
    
    var articleData = [Article]()
    let viewModel = ArticleViewModel()
    var currentPage = 0
    var isEndofData = false
    
    @IBOutlet var tableView : UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    /*
     Fetching article data
     */
    func fetchArticles(_ page : Int) {
            self.viewModel.fetchArticles(page, { (result) in
                switch result {
                case .failure(let error):
                    self.viewModel.isFetchInProgress = false
                    self.isEndofData = true
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                        self.show("Messsage", error.localizedDescription)
                    }
                    
                case .success(let articleData):
                    self.viewModel.isFetchInProgress = false
                    if let articleData = articleData {
                        if articleData.count > 0 {
                            self.articleData.append(contentsOf: articleData)
                        } else {
                            // end of pages
                            self.isEndofData = true
                        }
                    }
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
                
            })
//        }
    }
    
    func show(_ title : String, _ message : String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .cancel) { (action) in
            self.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    /*
     Setup Table view
     */
    func setupTableView() {
        self.tableView.separatorStyle = .singleLine
        self.tableView.register(UINib.init(nibName: articleCellIdentifier, bundle: nil), forCellReuseIdentifier: articleCellIdentifier)
        self.tableView.register(UINib.init(nibName: loadingCellIdentifier, bundle: nil), forCellReuseIdentifier: loadingCellIdentifier)
        self.tableView.layoutMargins = UIEdgeInsets.zero
        self.tableView.separatorInset = UIEdgeInsets.zero
    }
    
    //MARK :- UITableView Data source and delegate Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case Section.articleSection.rawValue:
            return self.articleData.count
        case Section.loadingSection.rawValue:
            return 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case Section.articleSection.rawValue:
            return articleTableViewCellEstimatedHeight
        case Section.loadingSection.rawValue:
            return loadingTableViewCellEstimatedHeight
        default:
            return 0
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case Section.articleSection.rawValue:
            return UITableView.automaticDimension
        case Section.loadingSection.rawValue:
            return loadingTableViewCellEstimatedHeight
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
            
        case Section.articleSection.rawValue:
            if let cell = tableView.dequeueReusableCell(withIdentifier: articleCellIdentifier) as? ArticleTableViewCell {
                
                let article = self.articleData[indexPath.row]
                if let user = article.user.anyObject() as? User {
                    cell.username.text = "\(user.name) " + "\(user.lastname)"
                    cell.designation.text = user.designation
                    if let url = URL(string: user.avatar) {
                        cell.profileImageView.sd_setImage(with: url, completed: nil)
                    }
                }
                if let media = article.media.anyObject() as? Media {
                    if let url = URL(string: media.image) {
                        cell.articleImageView.sd_setImage(with: url, completed: nil)
                    } else {
                        cell.articleImageViewHeightConstraint.constant = 0
                        cell.layoutIfNeeded()
                    }
                    cell.articleURL.setTitle(media.url, for: .normal)
                    cell.articleTitle.text = media.title
                }
                cell.contentText.text = article.content
                cell.likeButton.setTitle("\(viewModel.convertToReadable(article.likes.intValue)) Likes", for: .normal)
                cell.commentButton.setTitle("\(viewModel.convertToReadable(Int(truncating: article.comments))) Comments", for: .normal)
                cell.articlePublishedBefore.text = viewModel.convertToReadable(article.createdAt)
                
                cell.selectionStyle = .none
                cell.delegate = self
                cell.layoutMargins = UIEdgeInsets.zero
                return cell
            }
            
        case Section.loadingSection.rawValue:
            if let cell = tableView.dequeueReusableCell(withIdentifier: loadingCellIdentifier) as? LoadingTableViewCell {
                if !isEndofData {
                    cell.activityIndicator.startAnimating()
                    cell.loadingLabel.text = "\(loadingText) : \(self.currentPage + 1)"
                    self.currentPage = self.currentPage + 1
                    fetchArticles(self.currentPage)
                } else {
                    cell.activityIndicator.stopAnimating()
                    cell.loadingLabel.text = endOfLoadingText
                }
                cell.layoutMargins = UIEdgeInsets.zero
                return cell
            }
            
        default:
            return UITableViewCell()
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //action for cell selection
    }
}

extension ArticleListViewController : ArticleTableViewCellDelegate {
    func commentPressed(_ sender: UIButton) {
        // action for comments
    }
    
    func likePressed(_ sender: UIButton) {
        // action for like
    }
    
    func urlPressed(_ sender: UIButton) {
        // action for url
    }
}
