//
//  ArticleTableViewCell.swift
//  ArticleTestApp
//
//  Created by Ameya on 08/05/20.
//  Copyright © 2020 JTVC. All rights reserved.
//

import UIKit

/*
 Article Cell delegate
 */

protocol ArticleTableViewCellDelegate : class {
    func commentPressed(_ sender : UIButton)
    func likePressed(_ sender : UIButton)
    func urlPressed(_ sender : UIButton)
}

class ArticleTableViewCell: UITableViewCell {
    
    @IBOutlet var profileImageView : UIImageView!
    @IBOutlet var articleImageView : UIImageView!
    @IBOutlet var articlePublishedBefore : UILabel!
    @IBOutlet var username : UILabel!
    @IBOutlet var designation : UILabel!
    @IBOutlet var contentText : UILabel!
    @IBOutlet var articleTitle : UILabel!
    @IBOutlet var articleURL : UIButton!
    @IBOutlet var likeButton : UIButton!
    @IBOutlet var commentButton : UIButton!
    @IBOutlet var articleImageViewHeightConstraint : NSLayoutConstraint!
    
    weak var delegate : ArticleTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupProfileImageview()
        setupArticleImageview()
    }
    
    func setupProfileImageview() {
        self.profileImageView.layer.cornerRadius = profileImageView.frame.size.width / 2
        self.profileImageView.layer.masksToBounds = true
        self.profileImageView.layer.borderColor = UIColor.lightGray.cgColor
        self.profileImageView.layer.borderWidth = 1.0
    }
    
    func setupArticleImageview() {
        self.articleImageView.layer.borderColor = UIColor.lightGray.cgColor
        self.articleImageView.layer.borderWidth = 1.0
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    
    @IBAction func commentButtonPressed(_ sender : UIButton) {
        if let delegate = self.delegate {
            delegate.commentPressed(sender)
        }
    }
    @IBAction func likeButtonPressed(_ sender : UIButton) {
        if let delegate = self.delegate {
            delegate.likePressed(sender)
        }
    }
    @IBAction func articleUrlPressed(_ sender : UIButton) {
        if let delegate = self.delegate {
            delegate.likePressed(sender)
        }
    }
}


