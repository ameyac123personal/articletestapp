//
//  ArticleViewModel.swift
//  ArticleTestApp
//
//  Created by Ameya on 08/05/20.
//  Copyright © 2020 JTVC. All rights reserved.
//

import Foundation
import CoreData
import Network


class ArticleViewModel {
    
    let monitor = NWPathMonitor()
    var isConnected = false
    var isFetchInProgress = false
    
    /*
     Get article data page wise. 
     */
    
    init() {
        let queue = DispatchQueue(label: "Monitor")
        monitor.start(queue: queue)
        self.monitor.pathUpdateHandler = { path in
            if path.status == .satisfied {
                self.isConnected = true
            } else {
                self.isConnected = false
            }
        }
    }
    
    func fetchArticles(_ page : Int, _ completionHandler : @escaping ( Result<[Article]?, ArticleFetchError>) -> Void) {
        let group = DispatchGroup()
        group.enter()
        let queue = DispatchQueue.global()
        queue.async {
            if page == 1 {
                if self.isConnected {
                    self.clearAll()
                    group.leave()
                } else {
                    group.leave()
                }
            } else {
                group.leave()
            }
        }
        group.notify(queue: queue) {
            self.getArticleData(page, completionHandler)
        }
    }
    
    func getArticleData(_ page : Int, _ completionHandler : @escaping ( Result<[Article]?, ArticleFetchError>) -> Void) {
        
        let urlString = "https://5e99a9b1bc561b0016af3540.mockapi.io/jet2/api/v1/blogs?page=\(page)&limit=10"
        
        guard let url = URL(string: urlString) else {
            fatalError("Not a valid URL")
        }
        
        guard !isFetchInProgress else {
            return
        }
        isFetchInProgress = true
        
        let request = URLRequest(url: url)
        let sessionConfiguration = URLSessionConfiguration.default
        URLSession.init(configuration: sessionConfiguration).dataTask(with: request) { (data, response, error) in
            if let response = response as? HTTPURLResponse {
                if let data = data {
                    switch response.statusCode {
                    case 200:
                        let decoder = JSONDecoder()
                        decoder.userInfo[CodingUserInfoKey.context!] = self.persistentContainer.viewContext
                        do {
                            let articleData = try decoder.decode([Article].self, from: data)
                            do {
                                try self.persistentContainer.viewContext.save()
                                completionHandler(Result.success(articleData))
                            }catch {
                                completionHandler(Result.failure(ArticleFetchError.coredata))
                            }
                        }catch {
                            completionHandler(Result.failure(ArticleFetchError.decoding))
                        }
                    default:
                        completionHandler(Result.failure(ArticleFetchError.network))
                        break
                        
                    }
                }
            } else {
                let articleData = Article.getArticleData(page, context: self.persistentContainer.viewContext)
                if (articleData != nil) && (articleData!.count > 0) {
                    completionHandler(Result.success(articleData))
                } else {
                    completionHandler(Result.failure(ArticleFetchError.network))
                }
            }
        }.resume()
    }
    
    func clearAll() {
        Article.deleteAllArticles(self.persistentContainer.viewContext)
        User.deleteAllUsers(self.persistentContainer.viewContext)
        Media.deleteAllMedia(self.persistentContainer.viewContext)
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "ArticleTestApp")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    
}

extension ArticleViewModel {
    
    func convertToReadable(_ count : Int) -> String{
        if count > 999 {
            return "\(count / 1000)K"
        }
        return String(count)
    }
    
    func convertToReadable(_ date : NSDate) -> String {
        let dateComponent = Calendar.current.dateComponents([.hour], from: date as Date, to: Date())
        if let hours = dateComponent.hour {
            if hours > 24 {
                return "\(hours / 24) days"
            } else {
                return "\(hours) hr"
            }
        }
        return ""
    }
}
