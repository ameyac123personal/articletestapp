//
//  Article+CoreDataClass.swift
//  ArticleTestApp
//
//  Created by Ameya on 08/05/20.
//  Copyright © 2020 JTVC. All rights reserved.
//
//

import Foundation
import CoreData

extension CodingUserInfoKey {
    static let context = CodingUserInfoKey(rawValue: "context")
}

let dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
let dateFormatter = DateFormatter()


enum ArticleCodingKeys : String,CodingKey {
    case id
    case createdAt
    case content
    case comments
    case likes
    case media
    case user
}

enum ArticleFetchError : Error {
    case network
    case coredata
    case decoding
}

public class Article: NSManagedObject, Decodable {
    
    
    
    public override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }
    
    public required convenience init(from decoder: Decoder) throws {
        
        dateFormatter.dateFormat = dateFormat
        
        guard let context = CodingUserInfoKey.context, let managedObjectContext = decoder.userInfo[context], let entity = NSEntityDescription.entity(forEntityName: "Article", in: managedObjectContext as! NSManagedObjectContext) else {
            fatalError("")
        }
        self.init(entity : entity, insertInto : managedObjectContext as? NSManagedObjectContext)
        let values = try decoder.container(keyedBy: ArticleCodingKeys.self)
        do {
            
            id = try values.decode(String.self, forKey: .id)
            createdAt = try dateFormatter.date(from: values.decode(String.self, forKey: .createdAt))! as NSDate
            content = try values.decode(String.self, forKey: .content)
            comments = (try values.decode(Int.self, forKey: .comments)) as NSNumber
            likes = try (values.decode(Int.self, forKey: .likes)) as NSNumber
            media = NSSet(array: try values.decode([Media].self, forKey: .media))
            user = NSSet(array: try values.decode([User].self, forKey: .user))
            
        } catch  {
            print(error.localizedDescription)
        }
    }
    
    public class func getArticleData(_ page : Int, context : NSManagedObjectContext) -> [Article]? {
        let request = Article.fetchRequest(page)
        
        request.returnsObjectsAsFaults = false
        do {
            let articleData = try context.fetch(request)
            return articleData
        } catch {
            print(error.localizedDescription)
        }
        return nil
    }
    
    public class func deleteAllArticles(_ managedContext : NSManagedObjectContext)
    {
        do
        {
            let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: Article.fetchRequest())
            try managedContext.execute(batchDeleteRequest)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
}
