//
//  Media+CoreDataClass.swift
//  ArticleTestApp
//
//  Created by Ameya on 08/05/20.
//  Copyright © 2020 JTVC. All rights reserved.
//
//

import Foundation
import CoreData


enum MediaCodingKeys : String,CodingKey {
    case blogId
    case createdAt
    case id
    case image
    case title
    case url
}

public class Media: NSManagedObject, Decodable {
    
    
    public override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }
    
    public required convenience init(from decoder: Decoder) throws {
        
        guard let context = CodingUserInfoKey.context, let managedObjectContext = decoder.userInfo[context], let entity = NSEntityDescription.entity(forEntityName: "Media", in: managedObjectContext as! NSManagedObjectContext) else {
            fatalError("")
        }
        
        self.init(entity : entity, insertInto : managedObjectContext as? NSManagedObjectContext)
        let values = try decoder.container(keyedBy: MediaCodingKeys.self)
        
        do {
            id = try values.decode(String.self, forKey: .id)
            createdAt = try dateFormatter.date(from: values.decode(String.self, forKey: .createdAt))! as NSDate
            blogId = try values.decode(String.self, forKey: .blogId)
            image = try (values.decode(String.self, forKey: .image))
            title = try (values.decode(String.self, forKey: .title))
            url = try (values.decode(String.self, forKey: .url))
            
        } catch  {
            print(error.localizedDescription)
        }
    }
    
    public class func deleteAllMedia(_ managedContext : NSManagedObjectContext)
    {
        do
        {
            let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: Media.fetchRequest())
                try managedContext.execute(batchDeleteRequest)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
}
