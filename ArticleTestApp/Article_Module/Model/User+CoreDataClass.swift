//
//  User+CoreDataClass.swift
//  ArticleTestApp
//
//  Created by Ameya on 08/05/20.
//  Copyright © 2020 JTVC. All rights reserved.
//
//

import Foundation
import CoreData

enum UserCodingKeys : String,CodingKey {
    case avatar
    case about
    case blogId
    case city
    case createdAt
    case designation
    case id
    case lastname
    case name
}

public class User: NSManagedObject, Decodable {
    
    
    public override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }
    public required convenience init(from decoder: Decoder) throws {
        
        guard let context = CodingUserInfoKey.context, let managedObjectContext = decoder.userInfo[context], let entity = NSEntityDescription.entity(forEntityName: "User", in: managedObjectContext as! NSManagedObjectContext) else {
            fatalError("")
        }
        self.init(entity : entity, insertInto : managedObjectContext as? NSManagedObjectContext)
        let values = try decoder.container(keyedBy: UserCodingKeys.self)
        do {
            
            let request : NSFetchRequest<User> = User.fetchRequest()
            request.predicate = NSPredicate(format: "id == %@", try values.decode(String.self, forKey: .id))
            do {
                let result = try (managedObjectContext as! NSManagedObjectContext).fetch(request)
                if let resultingObject = result.last {
                    (managedObjectContext as! NSManagedObjectContext).delete(resultingObject)
                }
            } catch {
                print(error.localizedDescription)
            }

            self.about = try values.decode(String.self, forKey: .about)
            self.avatar = try values.decode(String.self, forKey: .avatar)
            self.blogId = try values.decode(String.self, forKey: .blogId)
            self.city = try (values.decode(String.self, forKey: .city))
            self.createdAt = try dateFormatter.date(from: values.decode(String.self, forKey: .createdAt))! as NSDate
            self.designation = try (values.decode(String.self, forKey: .designation))
            self.id = try (values.decode(String.self, forKey: .id))
            self.lastname = try (values.decode(String.self, forKey: .lastname))
            self.name = try (values.decode(String.self, forKey: .name))
            
        } catch  {
            print(error.localizedDescription)
        }
    }
    
    public class func deleteAllUsers(_ managedContext : NSManagedObjectContext)
     {
         do
         {
             let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: User.fetchRequest())
                 try managedContext.execute(batchDeleteRequest)
         } catch let error as NSError {
             print(error.localizedDescription)
         }
     }

}
