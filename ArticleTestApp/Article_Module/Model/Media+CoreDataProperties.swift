//
//  Media+CoreDataProperties.swift
//  ArticleTestApp
//
//  Created by Ameya on 08/05/20.
//  Copyright © 2020 JTVC. All rights reserved.
//
//

import Foundation
import CoreData


extension Media {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Media> {
        let request = NSFetchRequest<Media>(entityName: "Media")
        request.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]
        request.returnsObjectsAsFaults = true
        return request
    }
    
    @NSManaged public var blogId: String
    @NSManaged public var createdAt: NSDate
    @NSManaged public var id: String
    @NSManaged public var image: String
    @NSManaged public var title: String
    @NSManaged public var url: String
    @NSManaged public var media: Article
    
}
