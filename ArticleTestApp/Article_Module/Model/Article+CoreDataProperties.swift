//
//  Article+CoreDataProperties.swift
//  ArticleTestApp
//
//  Created by Ameya on 08/05/20.
//  Copyright © 2020 JTVC. All rights reserved.
//
//

import Foundation
import CoreData


extension Article {
    
    @nonobjc public class func fetchRequest(_ page : Int) -> NSFetchRequest<Article> {
        let request = NSFetchRequest<Article>(entityName: "Article")
        request.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]
        request.fetchLimit = 10
        request.fetchOffset = request.fetchLimit * (page - 1)
        return request
    }
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Article> {
        let request = NSFetchRequest<Article>(entityName: "Article")
        request.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]
        request.returnsObjectsAsFaults = true
        return request
    }
    
    @NSManaged public var comments: NSNumber
    @NSManaged public var content: String
    @NSManaged public var createdAt: NSDate
    @NSManaged public var id: String
    @NSManaged public var likes: NSNumber
    @NSManaged public var media: NSSet
    @NSManaged public var user: NSSet
    
}

// MARK: Generated accessors for media
extension Article {
    
    @objc(addMediaObject:)
    @NSManaged public func addToMedia(_ value: Media)
    
    @objc(removeMediaObject:)
    @NSManaged public func removeFromMedia(_ value: Media)
    
    @objc(addMedia:)
    @NSManaged public func addToMedia(_ values: NSSet)
    
    @objc(removeMedia:)
    @NSManaged public func removeFromMedia(_ values: NSSet)
    
}

// MARK: Generated accessors for user
extension Article {
    
    @objc(addUserObject:)
    @NSManaged public func addToUser(_ value: User)
    
    @objc(removeUserObject:)
    @NSManaged public func removeFromUser(_ value: User)
    
    @objc(addUser:)
    @NSManaged public func addToUser(_ values: NSSet)
    
    @objc(removeUser:)
    @NSManaged public func removeFromUser(_ values: NSSet)
    
}
