//
//  ArticleTestAppTests.swift
//  ArticleTestAppTests
//
//  Created by Ameya on 08/05/20.
//  Copyright © 2020 JTVC. All rights reserved.
//

import XCTest
import CoreData
@testable import ArticleTestApp

class ArticleTestAppTests: XCTestCase {

    var storeCoordinator: NSPersistentStoreCoordinator!
    var store: NSPersistentStore!
    var managedObjectContext: NSManagedObjectContext!
    var managedObjectModel: NSManagedObjectModel!

    override func setUp() {
        managedObjectModel = NSManagedObjectModel.mergedModel(from: nil)
        storeCoordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
        do {
            store = try storeCoordinator.addPersistentStore(ofType: NSInMemoryStoreType,
                                                        configurationName: nil, at: nil, options: nil)
        }catch {
            
        }
        managedObjectContext = NSManagedObjectContext.init(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = storeCoordinator

        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
            ArticleViewModel().getArticleData(1) { (result) in
                print(result)
            }
            
        }
    }

    func testThatStoreIsSetUp() {
      XCTAssertNotNil(store, "no persistent store")
    }

}
